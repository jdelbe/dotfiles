# Quick start

These one-liners install chezmoi in ~/.local/bin, then clone this repo.

### Read-only, do NOT apply anything yet

```bash
sh -c "$(curl -fsLS get.chezmoi.io/lb)" -- init https://gitlab.com/jdelbe/dotfiles.git
```

### (With valid SSH key) Trigger setup script, then apply everything

```bash
sh -c "$(curl -fsLS get.chezmoi.io/lb)" -- init --apply git@gitlab.com:jdelbe/dotfiles.git
```
