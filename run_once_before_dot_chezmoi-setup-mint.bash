#!/bin/bash
#############################################################################
#   Strict error handling
#############################################################################
set -o errexit   # abort on nonzero exit status
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes
# set -o xtrace    # for debugging

#############################################################################
#   Global constants
#############################################################################

#############################################################################
#   Functions
#############################################################################
# Trap function for ctrl+c handling
function user_interrupt() {
	echo
	echo "Cancelled by user (Ctrl+C)"
	exit 130   # exit status should be 128 + signal number
}

function setup_mint_common() {
	sudo apt update
	sudo apt install --yes nala
	sudo nala purge --yes \
		hexchat \
		mintchat \
		openvpn \
		pix \
		rhythmbox \
		sticky \
		transmission-common \
		# sentinel #
	sudo nala install --yes \
		bat \
		eza \
		fd-find \
		fzf \
		git \
		git-delta \
		glmark2-wayland \
		glmark2-x11 \
		gparted \
		guake \
		htop \
		meld \
		minicom \
		pdfarranger \
		ripgrep \
		rsync \
		sublime-text \
		tldr \
		vim \
		virt-manager \
		zoxide \
		# sentinel #

	# Permissions
	sudo usermod -a -G dialout $USER

	mkdir -p ~/.local/bin
	ln -s $(which batcat) ~/.local/bin/bat
	ln -s $(which fdfind) ~/.local/bin/fd

	# Atuin
	curl https://raw.githubusercontent.com/rcaloras/bash-preexec/master/bash-preexec.sh -o ~/.bash-preexec.sh
	curl --proto '=https' --tlsv1.2 -LsSf https://setup.atuin.sh | sh

	# Starship
	curl -sS https://starship.rs/install.sh | sh

	(
		cd /tmp

		# Draw.io
		curl -s https://api.github.com/repos/jgraph/drawio-desktop/releases/latest | rg browser_download_url.*amd64 | cut -d '"' -f 4 | wget -i -
		# Dust
		curl -s https://api.github.com/repos/bootandy/dust/releases/latest | rg browser_download_url.*amd64 | cut -d '"' -f 4 | wget -i -
		# Sad
		curl -s https://api.github.com/repos//ms-jpq/sad/releases/latest | rg browser_download_url.*x86_64-unknown-linux-gnu.deb | cut -d '"' -f 4 | wget -i -

		sudo dpkg --install *.deb
		rm *.deb
	)
}

function setup_cinnamon_pc() {
	setup_mint_common

	# Zed
	curl -f https://zed.dev/install.sh | sh

	(
		cd /tmp

		# Input-leap
		curl -s https://api.github.com/repos/input-leap/input-leap/releases/latest | rg browser_download_url.*ubuntu_24-04_amd64.deb | cut -d '"' -f 4 | wget -i -

		sudo dpkg --install *.deb
		rm *.deb
	)
}

function setup_mate_vm() {
	setup_mint_common

	sudo nala purge --yes \
		blueman \
		bluetooth \
		bluez \
		celluloid \
		drawing \
		hypnotix \
		libreoffice* \
		redshift \
		simple-scan \
		thunderbird* \
		# sentinel #
	sudo nala install --yes \
		apt-transport-https \
		bmap-tools \
		cutecom \
		dos2unix \
		ghex \
		git-filter-repo \
		iperf3 \
		lnav \
		pv \
		tftpd-hpa \
		# sentinel #

	# Locales (?)
	sudo locale-gen en_US.UTF-8

	# tftpd
	sudo install -d -m 0775 -o tftp -g tftp /srv/tftp
	sudo sed -i '/TFTP_OPTIONS/c\TFTP_OPTIONS="--secure --create"' /etc/default/tftpd-hpa
	sudo usermod -a -G tftp $USER

	# Disable CPU mitigations
	sudo sed -i 's/^GRUB_CMDLINE_LINUX_DEFAULT=""/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash mitigations=off"/' /etc/default/grub
	sudo update-grub
}

#############################################################################
#   Main
#############################################################################
trap user_interrupt INT   # always catch ctrl+c

# Source only 1 variable (Bashism)
. <( grep EDITION /etc/linuxmint/info )

case $(systemd-detect-virt) in
	none)
		echo "Normal PC (Desktop: $EDITION)"
		[ $EDITION = "Cinnamon" ] && setup_cinnamon_pc
		;;
	*)
		echo "VM (Desktop: $EDITION)"
		[ $EDITION = "MATE" ] && setup_mate_vm
		;;
esac
